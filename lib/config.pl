#!/usr/bin/perl

my %config = (

    # Application Name
    app_name            => 'ashutosh.dev',
    
    # Database Details
    db_host             => 'db_host',
    db_name             => 'PForums',
    db_user             => 'root',
    db_password         => '',

    #Email Settings
    smtp_server         => 'smtpserver',
    smtp_port           => 465,
    smtp_user           => 'smtpuser',
    smtp_password       => '',
);